package com.hotkidceo.springcloud.base;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * ceo ? y : n
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface IsLoggedinForCeo {
}
