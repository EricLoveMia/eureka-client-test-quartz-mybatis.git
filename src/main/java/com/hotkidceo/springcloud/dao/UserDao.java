package com.hotkidceo.springcloud.dao;

import java.util.List;

import com.hotkidceo.springcloud.domain.UserDO;

public interface UserDao {
	/**
	 * 
	* @Title: getAllUsers
	* @Description: 获取所有用户
	* @param @return    设定文件
	* @return List<UserDO>    返回类型
	* @throws
	 */
	List<UserDO> getAllUsers();
	
	/**
	 * 
	* @Title: insert
	* @Description: 插入一个用户
	* @param @param record
	* @param @return    设定文件
	* @return int    返回类型
	* @throws
	 */
	int insert(UserDO record);
	
	/**
	 * 
	* @Title: selectByPrimaryKey
	* @Description: 查询一个用户
	* @param @param id
	* @param @return    设定文件
	* @return UserDO    返回类型
	* @throws
	 */
	UserDO selectByPrimaryKey(Long id);
}
