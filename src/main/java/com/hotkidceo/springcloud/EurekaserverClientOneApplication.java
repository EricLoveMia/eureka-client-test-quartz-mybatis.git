package com.hotkidceo.springcloud;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hotkidceo.springcloud.base.IsLoggedinForCeo;
import com.hotkidceo.springcloud.domain.Ceo;

@EnableEurekaClient
@SpringBootApplication
@RestController
@EnableAutoConfiguration
@ImportResource("applicationContext.xml")
public class EurekaserverClientOneApplication {
	public static void main(String[] args) {
		SpringApplication.run(EurekaserverClientOneApplication.class, args);
	}
	
    @Value("${server.port}")
    String port;
    
    @RequestMapping("/login")
    public String login(@RequestParam String name) {
    	Ceo ceo = new Ceo();
        return "hi ceo"+name+",i am from port:" + port;
    }    
    
    @IsLoggedinForCeo
    @RequestMapping("/hi")
    public String home(@RequestParam String name) {
        return "hi ceo"+name+",i am from port:" + port;
    }
}

