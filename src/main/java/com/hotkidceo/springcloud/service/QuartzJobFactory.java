package com.hotkidceo.springcloud.service;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.hotkidceo.springcloud.domain.ScheduleJob;

@DisallowConcurrentExecution
public class QuartzJobFactory implements Job{

	private static final Logger LOG = Logger.getLogger(QuartzJobFactory.class.getName());
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.log(Level.INFO, "QuartzJobFactory execute start");
		ScheduleJob scheduleJob = (ScheduleJob)context.getMergedJobDataMap().get("scheduleJob");
		LOG.log(Level.INFO, "QuartzJobFactory execute end job name :" + scheduleJob.getJobName());
	}

}
