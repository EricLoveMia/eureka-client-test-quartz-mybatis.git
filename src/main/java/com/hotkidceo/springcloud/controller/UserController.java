package com.hotkidceo.springcloud.controller;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hotkidceo.springcloud.domain.UserDO;
import com.hotkidceo.springcloud.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	 private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	 
	 @Autowired
	 private UserService userService;

	 @Resource
	 private DiscoveryClient client;
	 
	    @RequestMapping(value="/addUser",method=RequestMethod.POST)
	    public int addUser(){
	    	UserDO user = new UserDO();
	    	user.setName("夏侯惇");
	    	user.setAddress("允州");
	    	user.setAge(30);
	    	user.setEmail("xiahoudun@sanguo.com");
	        return userService.addUser(user);
	    }

	    @RequestMapping(value="/getUserById",method=RequestMethod.GET)
	    public UserDO getUserById(@RequestParam Long id){
	        return userService.getUserById(id);
	    }

	    @RequestMapping(value="/getAllUsers",method=RequestMethod.GET)
	    public List<UserDO> getAllUsers(){
	        List<UserDO> listUser = userService.getAllUsers();
	        //输出服务信息
	        logger.info("result={}", listUser);
	        return listUser;
	    }
}
